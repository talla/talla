%%%
%%% Copyright (c) 2016 The Talla Authors. All rights reserved.
%%% Use of this source code is governed by a BSD-style
%%% license that can be found in the LICENSE file.
%%%
%%% -----------------------------------------------------------
%%% @author Alexander Færøy <ahf@0x90.dk>
%%% @doc Cowboy Middleware.
%%% @end
%%% -----------------------------------------------------------
-module(talla_dir_cowboy).

%% API.
-export([init/2]).

init(Req0, State) ->
    #{
        host := Host,
        port := Port,
        path := Path,
        version := Version
    } = Req0,
    lager:info("~s ~s ~s ~s", [Host, Port, Path, Version]),
    Req = cowboy_req:reply(200, #{
            <<"content-type">> => <<"text/plain">>
           }, <<"Hej du glade!">>, Req0),
    {ok, Req, State}.
